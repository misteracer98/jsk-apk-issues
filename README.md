# JSK APK Issues



## What is this

This is repository for users to add issues regarding APKs of JSK Unity games. If you experience crashes or other issues, create new issue so I can track them more easily. Click here https://gitlab.com/misteracer98/jsk-apk-issues/-/issues

The official forum for these APKs is on f95 zone: https://f95zone.to/threads/jsk-studio-games-2020-01-10-jsk-studios.24510/

The official destribution for these APKs is community repository: https://docs.google.com/document/d/1zKATq-fLY9RtIyRIhINzjdQnBf7U7CAHbEGos1aKxGs

If you have downloaded the APK from other sources, I'm not responsible for them. Please, in the future use f95 zone forum for updates and use our official links.

Many thanks for people in Translors Corner for helping making these APKs possible!
